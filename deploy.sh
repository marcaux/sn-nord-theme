#!/bin/sh
apt update
apt install zip
zip -r sn-nord-theme.zip ./ -x "package-lock.json" ".env" "*.sh" ".git*" ".git/*" "dist/.gitkeep" ".DS*" ".htaccess" "node_modules/*" "src/*" "*.txt"
mkdir public
mv sn-nord-theme.zip public/
cd public
unzip sn-nord-theme.zip
